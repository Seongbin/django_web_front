import * as types from './ActionTypes';

export const login = (id, token, userNumber) => {
  
    let result = {
        type : types.LOGIN,
        //type : types.MODE_READ_TWIT,
        account : {
            id,
            token,
            userNumber
        }
    }
    console.log("##resul11t",result)
    return result;
}

export const logout = () => {
    return {
        type : types.LOGOUT,
        account : {
            id : '',
            token : ''
        }
    }
}

/*
state = {
	twit_id serial,
	twit_text varchar(300),
	twit_content varchar,
	username character varying(150) not null,
	up_time timestamp default now(),
	primary key (twit_id),
	foreign key (username) references auth_user (username)
}
*/

export const writeTwit = (writeTwit) => {
    return {
        type : types.MODE_WRITE_TWIT,
        twit_text: writeTwit.twit_text,
        username : writeTwit.username
    }
}

export const getTwitList = () =>{
    return {
        type : types.MODE_READ_TWIT
    }
}