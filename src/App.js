import React from 'react';
import Root from './Root';

function App() {
  return (
    <React.Fragment>
      <Root/>
    </React.Fragment>
  );
}

export default App;