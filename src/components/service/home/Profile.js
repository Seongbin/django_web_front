import React,{useEffect, useState}  from 'react';
import {connect, useStore} from 'react-redux';
import axios from 'axios';


const idSelector = reduxState => {
    console.log("id Selector : ",reduxState);
    return reduxState.account.account.id
};

const tokenSelector = reduxState =>{
    console.log("accountSelect",reduxState);
    return reduxState.account.account.token
}

const userNumberSelector = reduxState => {
    return reduxState.account.account.userNumber
}

const Profile = (props) => {

    const [userInfo, setUserInfo] = useState('');
    
    useEffect(async () =>{
        getUserProfile(props.id, props.token , props.userNumber);
    },[]);

    const getUserProfile = (id, token, userNumber) => {

        //url 지정
        const URL = "http://127.0.0.1:8000/api/getUserProfile?id="+userNumber;

        //header 지정
        const config = {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                Authorization: "Token " + token
            }
        };

        //axios대응
        console.log("##config",config);
        return axios.get(URL, null, config).then(json =>{
            console.log(json.data)
            setUserInfo(json.data[0]);
            return json.data;
        });
    }
 
    return (
        <div className="column is-four-fifths is-offset-3">
            <div className="card">
                <div className="column">
                    <div className="card-image">
                        <figure className="image image is-3by1">
                            <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image" />
                        </figure>
                    </div>
                </div>
                <div className="card-content">
                    <div className="media">
                        <div className="media-left">
                            <figure className="image is-48x48">
                                <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image" />
                            </figure>
                        </div>
                        <div className="media-content">
                            <p className="title is-4">{userInfo.name}</p>
                            <p className="subtitle is-6">{props.id}</p>
                        </div>
                    </div>
                    <div className="content">
                    {userInfo.introduce}
                        {/* Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                        <a href="#">#css</a> <a href="#">#responsive</a> */}
                        <br />
                        {/* <time dateTime="2016-1-1">11:09 PM - 1 Jan 2016</time> */}
                        {userInfo.place}
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = reduxState => ({
    id : idSelector(reduxState),
    token : tokenSelector(reduxState),
    userNumber : userNumberSelector(reduxState)
});

export default connect(mapStateToProps, null) (Profile);
