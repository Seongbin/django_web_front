export const LOGIN = 'LOGIN';
export const LOGGEDIN = false;
export const LOGOUT = 'LOGOUT';
export const MODE_WRITE_TWIT = 'WRITETWIT';
export const MODE_READ_TWIT = 'READTWIT';