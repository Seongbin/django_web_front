import React ,{ useState }from 'react'
import '../../../css/css/all.css';

import TwitModal from './TwitModal';
import ModalPortal from '../../../shared/ModalPortal';
import '../../../css/css/all.css';

const TimeLine = ({twit_text, username}) => {
  
    const [modalFlg, setModalFlg] = useState(false);

    const setShowFlg = (showFlg) => {
        setModalFlg(showFlg);
    }

    return (
        <>
            <div className="column">
                <article className="media">
                    <figure className="media-left">
                        <p className="image is-64x64">
                            <img src="https://bulma.io/images/placeholders/128x128.png" />
                        </p>
                    </figure>
                    <div className="media-content">
                        <div className="content">
                            <p>
                                <strong>{username}</strong>
                            </p>
                            <div onClick={() => setModalFlg(true)} style={{whiteSpace: 'pre-line'}}>
                                 <p>
                                    {`${twit_text}`}
                                </p>
                            </div>
                            <p>
                                <small><a>Like</a> · <a>Reply</a> · 3 hrs</small>
                            </p>
                        </div>
                    </div>
                </article>
            </div>
            <ModalPortal>
                <TwitModal 
                    setShowFlg={setShowFlg}
                    showFlg={modalFlg}/>
            </ModalPortal>
        </>
    )
}

export default TimeLine;