import React from 'react';

import TwitterText from './twitter/TwitterText';
import TimeLineList from './twitter/TimeLineList';

const Twitter = () =>{
    return(
        <>
            <TwitterText />
            <TimeLineList/>
        </>
    )
}

export default Twitter;