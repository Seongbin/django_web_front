import React from 'react';
import { connect, useDispatch } from 'react-redux';
import { Redirect } from 'react-router-dom';

import * as actions from '../../redux/actions';
import getCookie from './GetCookie';

const Auth = (props) => { 
    const dispatch = useDispatch();
    let id = "";
    let token = "";
    let userNumber = "";

    let info  = getCookie();
    console.log("auth : ",info);
    if (info != false){
        id = info.account.id;
        token = info.account.token;
        userNumber = info.account.userNumber;
        dispatch(actions.login(id, token, userNumber));
    }

    //쿠키가 사라지면 세션도 사라지기 때문에 유저가 다시 로그인 하는걸로
    return(
        <>
            {(id ? props.children : <Redirect to={'/'}/>  )}
        </>
    )
}

const idSelector = (reduxState) =>{
    return reduxState.account.id;
}

const mapStateToProps = reduxState => ({
    id : idSelector(reduxState)
});

export default connect(mapStateToProps, null) (Auth);