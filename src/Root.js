import React from 'react';
import {BrowserRouter as Router, Route ,Switch} from 'react-router-dom';
import {createBrowserHistory} from 'history';

import LoginForm from './components/account/LoginForm';
import ServiceRoute from './components/service/ServiceRoute';
import Auth from './components/shared/Auth';
import SignUp from './components/account/SignUp'
import NotFound from './components/shared/NotFound'


const history = createBrowserHistory();

function Root() {
  return (
      <Router history={history}>
        <Switch>
          <Route path='/' exact component={LoginForm}/>
          <Route path='/signUp' exact component={SignUp}/>
            <Auth>
              <Switch>
                <Route path='/ServiceRoute' component={ServiceRoute} />
                <Route component={NotFound} />
              </Switch>
            </Auth>
          <Route component={NotFound} />
        </Switch>
      </Router>
  );
}

export default Root;