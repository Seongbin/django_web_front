import {combineReducers} from 'redux';

import account from './account';
import Twit from './Twit';

const reducers = combineReducers({
    account,Twit
})

export default reducers;