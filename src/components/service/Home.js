import React from 'react';

import Profile from './home/Profile';
import Twitter from './home/Twitter';
import Recommend from './home/Recommend';

import 'bulma/css/bulma.css';

const Home = () =>{
    return (
            <div className="columns">
                <div className="column is-one-quarter">
                    <Profile />
                </div>
                <div className="column">
                    <Twitter />
                </div>
                <div className="column is-one-quarter">
                    <Recommend />
                </div>
            </div>
    )
}
export default Home;