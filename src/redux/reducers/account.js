import * as types from '../actions/ActionTypes';

const initialState = {
    // loggedIn : false,
    // account : { 
    //     token : '',
    //     id : ''
    // }
}

const account = (state = initialState, action) => {

    switch(action.type){
        case types.LOGGEDIN :
        case types.LOGIN : 
            let result = {
                ...state,
                stateTwit : types.MODE_READ_TWIT,
                loggedIn : true,
                account : {
                    token : action.account.token,
                    id : action.account.id,
                    userNumber : action.account.userNumber
                }
            }
            console.log("result##",result);
            return result;
        case types.LOGOUT :
            return {
                ...state,
                loggedIn : false,
                account : {
                    token : '',
                    id : ''
                }
            }
        default :
            return state;
    }
}

export default account;