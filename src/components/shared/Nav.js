import React  from 'react';
import {Link} from 'react-router-dom';

import { useDispatch } from 'react-redux';
import useReactRouter from 'use-react-router';
import axios from 'axios';

import * as actions from '../../redux/actions/index';
import getCookie from './GetCookie';

import 'bulma/css/bulma.css';

const Nav = () => {

    const dispatch = useDispatch();
    const { history } = useReactRouter();

    const logout = (e) =>{
        e.preventDefault();
        let info = getCookie();
        console.log("cookie token ",info.account)
        logoutProcess(info.account);
        history.push("/")
    }

    const logoutProcess = async (info) =>{
        const URL = `http://127.0.0.1:8000/api/logout/`;
        let config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Token '+  info.token
              },
        }
    
        await axios.post(URL, info, config).then(() => {
            document.cookie = 'key' +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            dispatch(actions.logout());
       }).catch(err =>  {throw err} );
    }

    return (
        <div className="columns">
            <div className="column">
                <nav className="navbar is-primary" role="navigation" aria-label="main navigation">
                    <div className="navbar-brand">
                        <div className="navbar-item">
                            <Link to="/ServiceRoute">
                                <i className="fab fa-twitter-square"/>
                                &nbsp;FAKE Twitter
                            </Link>
                        </div>
                        <div role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                        </div>
                    </div>
                    <div className="navbar-menu">
                        <div className="navbar-start">
                            <div className="navbar-item">
                                test
                            </div>
                            <div className="navbar-item">
                                Moment
                            </div>
                            <div className="navbar-item">
                                <Link to="/ServiceRoute/Alert">Alert</Link>
                            </div>
                            <div className="navbar-item">
                                Facing
                            </div>
                        </div>
                        <div className="navbar-item is-pulled-right">
                            <button className="button is-info" onClick={logout}>logout</button>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    )
}

export default Nav;