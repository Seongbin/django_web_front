import React from 'react';

const TwitterFileUpload = ({getFile}) =>{

    const selectFile = e =>{
        e.preventDefault();
        getFile(null);
    }

    return (
        <>
            <div className="level-item">
                <div className="field">
                    <label className="file-label">
                        <input className="file-input" type="file" name="resume" onChange={selectFile}/>
                        <a className="button">
                            <span className="icon">
                            <i className='fas fa-image'></i>
                            </span>
                        </a>
                    </label>
                </div>
            </div>
            <div className="level-item">
                <div className="field">
                    <label className="file-label">
                        <input className="file-input" type="file" name="resume" onChange={selectFile}/>
                        <a className="button">
                            <span className="icon">
                                <i className="fas fa-play"></i>
                            </span>
                        </a>
                    </label>
                </div>
            </div>
        </>
    )
}

export default TwitterFileUpload;