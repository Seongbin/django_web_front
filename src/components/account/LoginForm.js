import React,{Component} from 'react';
import axios from 'axios';
import {connect} from 'react-redux';
import useReactRouter from 'use-react-router';

import * as actions from '../../redux/actions';

import 'bulma/css/bulma.css';

class LoginForm extends Component {
    constructor(){
        super();
        this.state = {
            username : '',
            userpassword : '',
            message : '',
            token: '',
            userNumber : ''
        }
    }

    handleInputChange = e => {
        this.setState({
            [e.target.name] : e.target.value
        });
        //이 구문은 당신이 무엇을 부르길 원하는지 미리 알지 못한 채 객체의 키를 설정하는 또 다른 방법이다.
        //https://stackoverflow.com/questions/49437859/adding-brackets-to-the-attribute-in-setstate
    }

    handleSubmit = e => {
    e.preventDefault();
    const { username, userpassword } = this.state;
    //변수 username 과 userpassword에 state값을 넣겠다

    const info = {
      username,
      userpassword
    };

    const URL = `http://127.0.0.1:8000/api/login/`;
    const MESSAGE = {
        CONNECT_FAIL : `Connection faild`,
        AUTHENTICATION_SUCCESS : `Login success! Welcome ${username}`,
        WRONG : `Wrong ID or Password Please try again`,
    };

    axios.post(URL, info)
        .then(res => {
            this.setState({
                message : MESSAGE.AUTHENTICATION_SUCCESS
            });
            let loginSuccess = async (setTime) =>{
                return await new Promise(resolve => {
                    if(resolve){
                        setTimeout( () =>{
                            this.props.history.push("/ServiceRoute")
                        } ,setTime)
                    }
                });
            }
            const time = 1 * 1000;
            loginSuccess(time);

            this.setState({
                token : res.data.tokenKey
            })


            this.setState({
                userNumber : res.data.userNumber
            })

            //token Key
            //발급받은 토큰은 저장해 놓으면서 서버쪽과 야리토리 하고
            //유효기간 지나면 리프레쉬
            let info = {
                account : {
                    id : this.state.username,
                    token : this.state.token,
                    userNumber : this.state.userNumber
                }
            }
            console.log("info_account_userNumber",info.account.userNumber)
            this.props.login(info.account.id, info.account.token, info.account.userNumber);

            document.cookie = 'key=' + btoa(JSON.stringify(info));
            //console.log("cookie : ",document.cookie);
            //console.log("id User: ",res.data.userNumber);
        })
        .catch(err => {
            if(!err.response){
                //https://stackoverflow.com/questions/47067929/how-to-handle-neterr-connection-refused-in-axios-vue-js
                this.setState({
                    message : MESSAGE.CONNECT_FAIL
                });
                return;
            }
            this.setState({
                message : MESSAGE.WRONG,
                username : '',
                userpassword : '',
            })
            document.querySelector('#username').focus()
        })
    };

    render(){
        const { username, userpassword } = this.state;
        const { handleInputChange, handleSubmit } = this;
        //함수작동을 위해서 넣어줌

        return (
            <form onSubmit={handleSubmit} className="container">
                <div className="columns is-mobile is-centered">
                    <div className="column is-one-quarter">
                        <fieldset>
                            <legend className="is-full is-centered has-text-centered">
                                <div className="column">
                                    like Twitter
                                </div>
                            </legend>
                            <div className="column">
                                <input
                                    className="input"
                                    type="text"
                                    name="username"
                                    id="username"
                                    placeholder="ID"
                                    onChange={handleInputChange}
                                    value={username}
                                    autoFocus/>
                            </div>
                            <div className="column">
                                <input
                                    className="input"
                                    type="password"
                                    name="userpassword"
                                    placeholder="password"
                                    onChange={handleInputChange}
                                    value={userpassword}/>
                            </div>
                            <div className="column">
                                {this.state.message}
                            </div>
                            <div className="column">
                                <button type="submit" className="button is-link is-fullwidth">LOGIN</button>
                            </div>
                            <div className="column">
                                <button type="button" className="button is-link is-fullwidth" onClick={
                                    (e) => {
                                        e.preventDefault();
                                        this.props.history.push("/SignUp")
                                    }
                                }>Sign Up </button>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </form>
        )
    }
}

const mapStateToProps = (reduxState) => {
    return {
        username: reduxState.id,
        token : reduxState.token 
    }
}

const mapDispatcherToProps = (dispatch) => {
    return {
        login : (id, token,userNumber) => {dispatch(actions.login(id, token, userNumber))},
    }
}

export default connect(mapStateToProps, mapDispatcherToProps) (LoginForm);