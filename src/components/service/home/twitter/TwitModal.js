import React from 'react';
import '../../../css/css/all.css';

const TwitModal = ({setShowFlg, showFlg}) =>{

    const close = e =>{
        e.preventDefault();
        showFlg = false;
        setShowFlg(showFlg);
    }

    return (
        <div className={`modal  ${showFlg ? "is-active" :"" } `}>
            <div className="modal-background"></div>
            <div className="modal-card">
                <header className="modal-card-head">
                    <p className="modal-card-title">Modal title</p>
                    <button className="delete" aria-label="close" onClick={close}></button>
                </header>
                <section className="modal-card-body">
                    test{/* <!-- Content ... --> */}
                </section>
                <footer className="modal-card-foot">
                    <button className="button is-success" onClick={close}>Save changes</button>
                    <button className="button" onClick={close}>Cancel</button>
                </footer>
            </div>
        </div>
    )
}

export default TwitModal;