import React,{useState} from 'react';
import axios from 'axios';
import {connect} from 'react-redux';

import * as actions from '../../../../redux/actions'

import 'bulma/css/bulma.css';
import '../../../css/css/all.css';

import TwitterFileUpload from './TwitterFileUpload';
import getCookie from '../../../shared/GetCookie';

const TwitterText = (props) => {

    const [text,textState] = useState("");

    const uploadTwit = (e) => {
        e.preventDefault();
        if(text == ""){
            return;
        }
        const cookieInfo = getCookie();
        upload(cookieInfo.account.token);
    }

    const upload = async (token) => {
        //url 지정
        const URL = "http://127.0.0.1:8000/api/uploadTwit/";

        //header 지정
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Token '+  token
              },
        }

        let writeTwit = new URLSearchParams();
        writeTwit.append('twit_text', text);
        writeTwit.append('username', props.id);
        //userTable 만들때까지 보류

        await axios.post(URL, writeTwit, config).then(
            json => {
                console.log(json);
            }
        ).catch(err => console.log(err));
        
        let writeTwitToComponent = {
            'twit_text' : text,
            'username' : props.id
        }
        props.write(writeTwitToComponent);
        textState("");
    }

    const getFile = (files) =>{
        console.log(files)
    }
    return (
        <div className="column">
            <article className="media">
                <figure className="media-left">
                    <p className="image is-64x64">
                        <img src="https://bulma.io/images/placeholders/128x128.png" />
                    </p>
                </figure>
                <div className="media-content">
                    <div className="field">
                        <p className="control">
                            <textarea 
                                className="textarea is-small has-fixed-size" 
                                rows="3" 
                                placeholder="Add a comment..."
                                value={text}
                                onChange = {e => textState(e.target.value)} />
                        </p>
                    </div>
                    <nav className="level">
                        <div className="level-left">
                            <TwitterFileUpload 
                                getFile={getFile}/>
                        </div>
                        <div className="level-right">
                            <div className="level-item" onClick={uploadTwit}>
                                <label className="checkbox">
                                    <a className="button is-info">Submit</a>
                                </label>
                            </div>
                        </div>
                    </nav>
                </div>
                <div className="media-right">
                    <a className="button">
                        <span className="icon">
                            <i className="fas fa-smile"></i>
                        </span>
                    </a>
                </div>
            </article>
        </div>
    )
}

const mapStateToProps = (reduxState) =>{
    return{
        id : idSelector(reduxState)
    } 
}

const mapDispatcherToProps = (dispatch) =>{
    return{
        write : (writeTwit) => {dispatch(actions.writeTwit(writeTwit))}
    }
}

const idSelector = (reduxState) => {
    return reduxState.account.account.id;
}

export default connect(mapStateToProps, mapDispatcherToProps) (TwitterText);