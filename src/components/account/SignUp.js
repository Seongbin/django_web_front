import React, {useState} from 'react'; // import useState
import axios from 'axios';

import 'bulma/css/bulma.css';

const config = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
}

const SignUp = (props) => {
    const [id, idState] = useState("");
    const [password, passwordState] = useState("");
    const [checkPassword, checkPasswordState] = useState("");
    const [passwordMessage, passwordMessageState] = useState("");
    const [idMessage, idMessageState] = useState("")
    const [message, messageState] = useState("");

    const messageList = [
        'Please check Password',
        'Please check register form',
        'ID is already registered'
    ]
    const checkId = async (value) => {
        const URL = "http://127.0.0.1:8000/api/idCheck"
    
        let userInput = new URLSearchParams();
        idState(value);
        userInput.append('userId', id);

        await axios.post(URL,userInput,config)
            .then(json => {
                    if(1 <= json.data.length){
                        idMessageState(messageList[2]);
                    }else if(id == ''){
                        idMessageState("");
                    }else{
                        idMessageState("");
                    }
                    messageCheck();
            }).catch(e => console.log(e));;
    }

    const isNotSamePassword = () => {
        if(password !== checkPassword){
            passwordMessageState(messageList[0])
        }else{
            passwordMessageState('')
        }
        messageCheck();
    }

    const signUpSubmit = async () =>{
        if(idMessage == messageList[2] || passwordMessage == messageList[0]){
            messageState(messageList[1])
            return;
        }

        const URL = "http://127.0.0.1:8000/api/signUp";

        let userInput = new URLSearchParams();
        userInput.append("userId", id);
        userInput.append("password", password);
        await axios.post(URL, userInput, config)
            .then(json => {
                props.history.push("/")
        }).catch(e => console.log(e));
    }

    const messageCheck = () => {
        if(idMessage == "" || passwordMessage == ""){
            messageState("")
        }
    }
    
    return (
        <>
            <div className="columns is-mobile is-centered">
                <div className="column is-one-quarter">
                    <div className="column has-text-centered">
                        SignUp
                    </div>
                    <div className="field">
                        <label className="label" htmlFor="id">ID</label>
                        <div className="control">
                            <input
                                className="input"
                                type="text"
                                id="id"
                                placeholder="ID"
                                value ={id}
                                onKeyUp = {e =>{
                                    e.preventDefault();
                                    checkId(e.target.value)
                                }}
                                onChange={e => {
                                    e.preventDefault();
                                    idState(e.target.value)
                                }
                            }/>
                        </div>                  
                    </div>
                    <div className="field">
                        <label className="label" htmlFor="password">Password</label>
                        <div className="control">
                            <input 
                                type="password"
                                id="password"
                                className="input"
                                placeholder="password"
                                value={password}
                                onKeyUp={ e => {
                                        e.preventDefault();
                                        isNotSamePassword()
                                    }
                                }
                                onChange={e => passwordState(e.target.value)}/>
                        </div>                  
                    </div>
                    <div className="field">
                        <label className="label" htmlFor="checkPassword">Check Password</label>
                        <div className="control">
                            <input 
                                type="password"
                                id="checkPassword"
                                placeholder="Check Password"
                                className="input"
                                value={checkPassword}
                                onKeyUp={ e => {
                                        e.preventDefault();
                                        isNotSamePassword()
                                    }
                                }
                                onChange={e => checkPasswordState(e.target.value)}/>
                        </div>
                    </div>
                    <input
                        type="button"
                        className="button is-link is-fullwidth"
                        value="Sign Up"
                        onClick={ e => {
                            e.preventDefault();
                            signUpSubmit()
                           }
                        }/>
                    <div className="field">
                        {idMessage}
                    </div>
                    <div className="field">
                        {passwordMessage}
                    </div>
                    <div className="field">
                        {message}
                    </div>
                </div>
            </div>
        </>
    );
};

export default SignUp;