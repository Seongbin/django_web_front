import ReactDOM from 'react-dom';

const ReactPortal = ({children}) =>{
    const el = document.querySelector("#modal");
    return ReactDOM.createPortal(children, el);
}

//https://velog.io/@velopert/react-portals
export default ReactPortal;