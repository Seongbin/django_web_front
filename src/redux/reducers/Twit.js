import * as types from '../actions/ActionTypes';
const initialState = [];

const Twit = (state = initialState, action) => {

    switch(action.type){
        
        case types.MODE_WRITE_TWIT:
            return [
                ...state,
                {
                    twit_text : action.twit_text,
                    username : action.username,
                    stateTwit : types.MODE_WRITE_TWIT
                }
            ];
        case types.MODE_READ_TWIT:
            return [
                ...state,
                {
                    twit_text : action.twit_text,
                    username : action.username,
                    stateTwit : types.MODE_READ_TWIT
                }
            ];
        default:
            return state;
    }
}

export default Twit;