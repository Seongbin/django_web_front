import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

import TimeLine from './TimeLine';
import * as types from '../../../../redux/actions/ActionTypes';
import axios from 'axios';

// timeLine List
// 1. 일단 서버로부터 타임라인 값을 가지고 온다
// 2. 타임라인 값 없으면 없다고 표시
// 3. 타임라인 리스트를 작성한다
// 4. 값을 받으면 타임라인 컴포넌트를 생성한다

/*
async 안에 await을 넣을것

const f = async () => {
  await bar();
}

async function hoge() {
  await fetch();
}

async function something(){
  await something;
}

*/

const TimeLineList = (props) => {
    const [twitList, setTwitList] = useState(null);

    useEffect(async () => {
        getTwit();
    }, []);


    const getTwit = async () => {
        let twitList = null;

        if (props.stateTwit === types.MODE_READ_TWIT) {
            twitList = await getTwitList(props.stateTwit);
        } else {
            twitList = addTwitWhenRead();
        }

        if (twitList != null) {
            setTwitList({
                ...twitList,
                param: twitList,
            })
        }
    }

    const getTwitList = () => {

        //token 지정
        const token = props.account.account.token

        //url 지정
        const URL = "http://127.0.0.1:8000/api/getAllTimeLine/";

        //header 지정
        const config = {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                Authorization: "Token " + token
            }
        };

        //axios대응
        return axios.post(URL, null, config)
            .then((json) => {
                return json.data;
            })
            .catch(err => console.log(err));
    };

    const renderTimeLine = () => {
        console.log(twitList);
        return twitList.param.map(twit => {
            return <TimeLine
                twit_text={twit.twit_text}
                username={twit.username} />
        })
    }

    const addTwitWhenRead = param => {
        console.log("param", param)
        if (param) {
            setTwitList({
                ...twitList,
                param
            });
        }
    };

    return (
        <>
            {
                twitList == null ? "loading" : renderTimeLine()
            }
        </>
    )
}

const mapStateToProps = (reduxState) => {
    return {
        twitList: getTwit(reduxState),
        account: getAccountInfo(reduxState),
        stateTwit: getStateTwit(reduxState),
    }
}

const getTwit = (reduxState) => {
    return reduxState.Twit;
}

const getAccountInfo = (reduxState) => {
    return reduxState.account;
}

const getStateTwit = (reduxState) => {
    console.log("reduxState : ", reduxState.account)
    return reduxState.account.stateTwit
}

export default connect(mapStateToProps, null)(TimeLineList);