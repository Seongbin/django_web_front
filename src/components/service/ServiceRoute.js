import React from 'react';
import {Route, Switch} from 'react-router-dom';

import Nav from '../shared/Nav';

import Alert from './Alert';
import Home from './Home';
import NotFound from '../shared/NotFound'

const ServiceRoute = ({match}) => {
    return(
        <>
            <header>
                <Nav/>
            </header>
            <Switch>
                <Route path={match.path} exact component={Home}/>
                <Route path={match.path+'/Alert'} exact component={Alert} />
            </Switch>
        </>
    )
}

export default ServiceRoute;

//https://stackoverflow.com/questions/55238776/how-to-implement-nested-routing-child-routes-in-react-router-v4